import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:pokedex/models/simple_pokemon/simple_pokemon.dart';

part 'simple_pokemon_list.freezed.dart';

part 'simple_pokemon_list.g.dart';

@freezed
abstract class SimplePokemonList with _$SimplePokemonList {
  factory SimplePokemonList({
    @required @JsonKey(name: 'count') int count,
    @required @nullable @JsonKey(name: 'next', nullable: true) String next,
    @required
    @nullable
    @JsonKey(name: 'previous', nullable: true)
        String previous,
    @required @JsonKey(name: 'results') List<SimplePokemon> simplePokemonList,
  }) = _SimplePokemonList;

  factory SimplePokemonList.fromJson(Map<String, dynamic> json) =>
      _$SimplePokemonListFromJson(json);
}
